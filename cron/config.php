<?php
//id's of all countries
define(WORLD_COUNTRIES_PATH, dirname(__FILE__).'/../assets/static/json/worldmap.json');

//place where stored data for charts
define(PATH_TO_SAVE, dirname(__FILE__).'/../assets/charts-data/json/');

//general info by all countries
define(ALL_COUNTRIES_INFO, 'https://corona-api.com/countries'); 

$chinaPathes = array('cn');
$europPathes = array('nl', 'be', 'lu', 'de', 'pl', 'cz', 'sk', 'ch', 'li', 'at', 'hu', 'ro', 
					 'md', 'bg', 'gr', 'al', 'mk', 'me', 'rs', 'ba', 'hr', 'si', 'it', 'va', 
					 'sm', 'mc', 'fr', 'ad', 'es', 'pt', 'is', 'no', 'se', 'fi', 'ru', 'ee', 
					 'lv', 'ie', 'gb', 'dk', 'lt', 'by', 'ua', 'mt'
);
$americasPathes = array('ar', 'bo', 'br', 'cl', 'co', 'ec', 'fk', 'gf', 'gy', 'py', 'pe', 
					 'gs', 'sr', 'uy', 've', 'ag', 'aw', 'bs', 'bb', 'bz', 'bm', 'bq', 'vg', 
					 'ca', 'ky', 'cr', 'cu', 'cw', 'dm', 'do', 'sv', 'gd', 'gp', 'gt', 'ht', 
					 'hn', 'jm', 'mq', 'mx', 'ms', 'ni', 'pa', 'pr', 'kn', 'lc', 'mf', 'pm', 
					 'vc', 'sx', 'tt', 'tc', 'us', 'vi'
);
$otherPathes = array('gl', 'dz', 'ao', 'bj', 'bw', 'bf', 'bi', 'cm', 'cv', 'cf', 'td', 'km',
					 'ci', 'cd', 'dj', 'eg', 'gq', 'er', 'et', 'ga', 'gh', 'gn', 'gw', 'ke',
					 'ls', 'li', 'lr', 'mw', 'ml', 'mr', 'ma', 'mz', 'na', 'ne', 'ng', 'rw',
					 'st', 'sn', 'sc', 'sl', 'so', 'za', 'sd', 'sz', 'tz', 'tg', 'tn', 'ug',
					 'eh', 'zm', 'zw', 'gm', 'cg', 'mu', 'af', 'am', 'az', 'bd', 'bt', 'bn',
					 'mm', 'kh', 'cn', 'tl', 'ge', 'in', 'id', 'ir', 'jp', 'kz', 'kp', 'kr',
					 'kg', 'la', 'my', 'mn', 'np', 'pk', 'ph', 'sg', 'lk', 'tj', 'th', 'tm',
					 'uz', 'vn', 'cy', 'tr', 'au', 'fj', 'ki', 'mh', 'fm', 'nr', 'nz', 'pw',
					 'pg', 'ws', 'sb', 'to', 'tv', 'vu', 'nc', 'bh', 'il', 'jo', 'kw', 'om',
					 'qa', 'sa', 'sy', 'ae', 'ye', 'pr', 'ss', 'ai', 'as', 'cc', 'ck', 'cx',
					 'cw', 'fo', 'pf', 'gi', 'gu', 'gp', 'gg', 'im', 'mv', 'mp', 'nf', 'nu',
					 'pn', 're', 'sh', 'ps', 'wf', 'tk'
);
$apiPathes = array(
    //general info by all countries
    array('path' => 'https://corona-api.com/countries', 'name' => 'Map'),
	//daily gospitalizations by mainlands
    array('path' => 'https://corona-api.com/countries',
		'keys' => array(
				'China' => $chinaPathes, 
				'Outside China' => array_merge($europPathes, $americasPathes, $otherPathes),
				'Europe' => $europPathes,
				'Americas' => $americasPathes,
				'Other' => $otherPathes
			),
			'name' => 'PerRegionsValues'),
	// Cumulative New cases / Infections / Deaths
    array('path' => 'https://corona-api.com/countries',
		'keys' => array(
				'All' => array_merge($chinaPathes, $europPathes, $americasPathes, $otherPathes),
			),
			'name' => 'CumulativeValues'),
);