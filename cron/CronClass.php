<?php 
class CronClass {

	const WORLD_COUNTRIES_PATH = WORLD_COUNTRIES_PATH;
	const PATH_TO_SAVE = PATH_TO_SAVE;
	
	public $worldCountries;
	
	function __construct() {
		$this->setWorldCountriesData();
	}
	
	function getWorldCountriesData()
	{
		return $this->worldCountries;
	}
	
    public function getPathSave()
    {
        return self::PATH_TO_SAVE;
    }

	public function saveFile($data, $fname)
	{
		$fileName = $this->getPathSave().$fname;
		$fh = fopen($fileName, "w+") or die("Unable to open file! " .$fileName);
		if (!is_writable($fileName)) {die("Unable write to file! " .$fileName);}
        fwrite($fh, $data);
		fclose($fh);
        echo 'File '.$fname.' updated successful!'."\n";
	}
	
	public function run($pathes)
	{
        if ($pathes && sizeof($pathes)) {
            echo 'Runned - data acquisition started' . "\n";
            foreach ($pathes as $item) {
				if ($item['path']) {
					$methodName = 'makedataFor'.$item['name'];
					if (method_exists($this,  $methodName)) {
						$this->$methodName($item);
					}
				}
            }
            echo 'Completed - data acquisition finished' . "\n";
            
            return true;
        }
     
        echo 'An error occurred at startup' . "\n";
        return false;
	}

    function getJSONData($path)
    {
 		if ($jsonData = file_get_contents($path)) {
            if (NULL !== $decodedData = json_decode($jsonData)) {
                if (is_object($decodedData) && sizeof($decodedData)) {
                    return $decodedData;
                }
            }
        }

        return false;
    }


/*===========================================*/
	//Map
    public function getPathToCountriesIDs()
    {
        return self::WORLD_COUNTRIES_PATH;
    }

	function setWorldCountriesData()
	{
		if ($worldCountries = $this->getJSONData($this->getPathToCountriesIDs())) {
            $this->worldCountries = $worldCountries;
        }

        return false;
	}
	
	function findCountryID($code)
	{
		foreach ($this->getWorldCountriesData()->entities as $country) {
			if ($country->shortLabel == $code) {
				return $country->id;
			}
		}
        
        return false;
	}

	function makedataForMap($path)
	{
		if ($countries = $this->getJSONData($path['path'])) {
			$optimizedCountrisData = array();
			foreach ($countries->data as $index => $country) {
				$cadata = array();
				if ($country->latitude !== 0 && $country->longitude !== 0) {
					$countryID = $this->findCountryID($country->code);
					$cadata['shortLabel'] = $country->code;
					if ($country->latest_data->calculated->cases_per_million_population) {
						$cadata['value'] = strval($country->latest_data->calculated->cases_per_million_population);
					} else {
						$cadata['value'] = 'No data';
						//$cadata['tooltext'] = 'No data available';
					}
					if (is_numeric($countryID)) {
						$cadata['id'] = strval($countryID);
					}
					$optimizedCountrisData[$index] = (object)$cadata;
				}
			}
			
			if ($optimizedCountrisData && sizeof($optimizedCountrisData)) {
				$this->saveFile(json_encode($optimizedCountrisData), 'mapData');
			}
		}
	}
	
/*===========================================*/
	//Gospitalizations / Global deaths / Case fatality rate
	protected function makedataForPerRegionsValues($pathesList) {
		$dateList = $this->getDatesList();
		$dataSetActive = $dataSetDeath = $dataSetFatalityRate = array(); $index = 1;
		foreach ($pathesList['keys'] as $keysName => $keys) {
			$dataActive = $dataDeath = $dataFatalityRate = array();
			foreach ($keys as $key) {
				if ($this->getJSONData($pathesList['path'].'/'.$key)) {
					if ($dataByPathKey = array_reverse($this->getJSONData($pathesList['path'].'/'.$key)->data->timeline)) {
						$valueActive = $valueDeath = $valueFatalityRate = 0;
						foreach ($dateList as $date) {
							if ($newValue = $this->searchValue($dataByPathKey, $date->label)) {
								$valueActive = $newValue->active; // Gospitalizations
								$valueDeath = $newValue->deaths; // Global death
								$valueFatalityRateDeaths = $newValue->deaths; // Deaths
								$valueFatalityConfirmed = $newValue->confirmed; // Confirmed
								if ($keysName == 'Outside China' && 1584316799 < strtotime($date->label)) {
									$valueActive = $valueDeath = '';
								}
								if (($keysName == 'Other' || $keysName == 'Americas' || $keysName == 'Europe') && 1584316799 > strtotime($date->label)) {
									$valueActive = $valueDeath = '';
								}
							}
							// Gospitalizations
							if (!array_key_exists($date->label, $dataActive)) {
								$dataActive = array_merge($dataActive, array($date->label => $valueActive));
							} else {
								$dataActive[$date->label] = $dataActive[$date->label] + $valueActive;
							}
							// Global death
							if (!array_key_exists($date->label, $dataDeath)) {
								$dataDeath = array_merge($dataDeath, array($date->label => $valueDeath));
							} else {
								$dataDeath[$date->label] = $dataDeath[$date->label] + $valueDeath;
							}
							// Fatality rate
							if (!array_key_exists($date->label, $dataFatalityRate)) {
								$valueFatalityRatio = $valueFatalityConfirmed ? $valueFatalityRateDeaths / $valueFatalityConfirmed * 100 : 0;
								$dataFatalityRate = array_merge($dataFatalityRate, array($date->label => array(death => $valueFatalityRateDeaths, confirmed => $valueFatalityConfirmed, ratio => $valueFatalityRatio)));
							} else {
								$dataFatalityRate[$date->label][death] = $dataFatalityRate[$date->label][death] + $valueFatalityRateDeaths;
								$dataFatalityRate[$date->label][confirmed] = $dataFatalityRate[$date->label][confirmed] + $valueFatalityConfirmed;
								$valueFatalityRatio = $dataFatalityRate[$date->label][death] / $dataFatalityRate[$date->label][confirmed] * 100;
								$dataFatalityRate[$date->label][ratio] = $valueFatalityRatio;
							}
						}
					}
				} else {
				    $this->writeLog('makedataForPerRegionsValues = ' .$pathesList['path'].'/'.$key);
				}
			}
			// Gospitalizations
			if (!empty($dataActive)) {
				$dataSetActive[] = (object)array(seriesname => $keysName, color => $this->setBarColor($keysName), data => $this->prepareData($dataActive));
			}
			// Global death
			if (!empty($dataDeath)) {
				$dataSetDeath[] = (object)array(seriesname => $keysName, color => $this->setBarColor($keysName), data => $this->prepareData($dataDeath));
			}
			// Fatality rate
			if (!empty($dataFatalityRate)) {
				$dataSetFatalityRate[] = (object)array(seriesname => $keysName, color => $this->setBarColor($keysName), data => $this->prepareFatalityRateData($dataFatalityRate));
			}
			$index++;
		}
		$category = $this->getDatesList('M j');
		// Gospitalizations
		if ($category && $dataSetActive && sizeof($category) && sizeof($dataSetActive)) {
			$this->saveFile(json_encode(array('categories' => array(array('category' => $category)), 'dataset' => $dataSetActive)), 'gospitalizationsData');
		}
		// Global death
		if ($category && $dataSetDeath && sizeof($category) && sizeof($dataSetDeath)) {
			$this->saveFile(json_encode(array('categories' => array(array('category' => $category)), 'dataset' => $dataSetDeath)), 'globalDeathsData');
		}
		// Fatality rate
		if ($category && $dataSetFatalityRate && sizeof($category) && sizeof($dataSetFatalityRate)) {
			$this->saveFile(json_encode(array('categories' => array(array('category' => $category)), 'dataset' => $dataSetFatalityRate)), 'fatalityRateData');
		}
	}
	
	function getDatesList($format = 'Y-m-d')
	{
		$dateList = array();
		date_default_timezone_set('UTC');

		// Start date
		$date = strtotime('2020-01-22');
		// End date
		$endDate = strtotime('now');

		while ($date <= $endDate) {
			$dateList[] = (object)array(label => date($format, $date));
			$date = strtotime("+1 day", $date);
		}

		if ($dateList && sizeof($dateList)) {
			return $dateList;
		}
		
		return false;
	}
	
	function searchValue($data, $value)
	{
		foreach ($data as $item) {
			if ($item->date == $value) {
				return $item;
				//return $item->active;
			}
		}
		
		return false;
	}
	
	function prepareData($data)
	{
		$returnData = array();
		foreach ($data as $item) {
			$returnData[] = (object)array(value => $item);
		}
		
		return !empty($returnData) ? $returnData : false;
	}
	
	function setBarColor($areaName)
	{
		switch($areaName) {
			case 'China': return '#494949';
			case 'Outside China': return '#c8435e';
			case 'Americas': return '#41a0bc';
			case 'Europe': return '#26af69';
			case 'Other': return '#858585';
		}
	}

/*===========================================*/
// FatalityRate	
/*	protected function makedataForFatalityRate($pathesList) {
		$dateList = $this->getDatesList();
		$dataSet = $currDataSet = array(); $index = 1;
		foreach ($pathesList['keys'] as $keysName => $keys) {
			$data = array();
			foreach ($keys as $key) {
				if ($this->getJSONData($pathesList['path'].'/'.$key)) {
					if ($dataByPathKey = array_reverse($this->getJSONData($pathesList['path'].'/'.$key)->data->timeline)) {
						$death = $confirmed = 0;
						foreach ($dateList as $date) {
							if ($valuesByDate = $this->searchValue($dataByPathKey, $date->label)) {
								$death = $valuesByDate->deaths;
								$confirmed = $valuesByDate->confirmed;
							}
							if (!array_key_exists($date->label, $data)) {
								$ratio = $death / $confirmed * 100;
								$data = array_merge($data, array($date->label => array(death => $death, confirmed => $confirmed, ratio => $ratio)));
							} else {
								$data[$date->label][death] = $data[$date->label][death] + $death;
								$data[$date->label][confirmed] = $data[$date->label][confirmed] + $confirmed;
								$ratio = $data[$date->label][death] / $data[$date->label][confirmed] * 100;
								$data[$date->label][ratio] = $ratio;
							}
						}
					}
				} else {
					$this->writeLog('makedataForFatalityRate = ' .$pathesList['path'].'/'.$key);
				}
			}
				$dataSet[] = (object)array(seriesname => $keysName, color => $this->setBarColor($keysName), data => $this->prepareFatalityRateData($data));
			}

			if (!empty($data)) {
			$index++;
		}
		
		if ($dateList && $dataSet && sizeof($dateList) && sizeof($dataSet)) {
			$this->saveFile(json_encode(array('categories' => array(array('category' => $dateList)), 'dataset' => $dataSet)), 'fffffatalityRateData');
		}
	}
*/
	function prepareFatalityRateData($data)
	{
		$returnData = array();
		foreach ($data as $item) {
			$returnData[] = (object)array(value => $item[ratio]);
		}
		
		return !empty($returnData) ? $returnData : false;
	}
	
/*===========================================*/
// Infections

	protected function makedataForInfectionsRegions($pathesList) {
		$dataSet = $currDataSet = array(); $index = 1;
		foreach ($pathesList['keys'] as $keysName => $keys) {
			$data = array();
			foreach ($keys as $key) {
				if ($this->getJSONData($pathesList['path'].'/'.$key)) {
					if ($dataByPathKey = array_reverse($this->getJSONData($pathesList['path'].'/'.$key)->data->timeline)) {
						$death = $confirmed = 0;
						foreach ($dateList as $date) {
							if ($valuesByDate = $this->searchValue($dataByPathKey, $date->label)) {
								$death = $valuesByDate->deaths;
								$confirmed = $valuesByDate->confirmed;
							}
							if (!array_key_exists($date->label, $data)) {
								$ratio = $death / $confirmed * 100;
								$data = array_merge($data, array($date->label => array(death => $death, confirmed => $confirmed, ratio => $ratio)));
							} else {
								$data[$date->label][death] = $data[$date->label][death] + $death;
								$data[$date->label][confirmed] = $data[$date->label][confirmed] + $confirmed;
								$ratio = $data[$date->label][death] / $data[$date->label][confirmed] * 100;
								$data[$date->label][ratio] = $ratio;
							}
						}
					}
				} else {
					$this->writeLog('makedataForInfectionsRegions = ' .$pathesList['path'].'/'.$key);
				}
			}
				$dataSet[] = (object)array(seriesname => $keysName, color => $this->setBarColor($keysName), data => $this->prepareFatalityRateData($data));
			}

			if (!empty($data)) {
			$index++;
		}
		
		if ($dateList && $dataSet && sizeof($dateList) && sizeof($dataSet)) {
			$this->saveFile(json_encode(array('categories' => array(array('category' => $dateList)), 'dataset' => $dataSet)), 'infectionsRegionsData');
		}
	}
	
	
/*===========================================*/
// Infections

	protected function makedataForReproduction($pathesList) {
		
	}

/*===========================================*/

// New cases / Infections / Deaths
	protected function makedataForCumulativeValues($pathesList) {
		$dateList = $this->getDatesList();
		$dataSetNew = $dataSetInfections = $dataSetDeath = array(); $index = 1;
		foreach ($pathesList['keys'] as $keysName => $keys) {
			$dataNew = $dataInfections = $dataDeaths = array();
			foreach ($keys as $key) {
				if ($this->getJSONData($pathesList['path'].'/'.$key)) {
					if ($dataByPathKey = array_reverse($this->getJSONData($pathesList['path'].'/'.$key)->data->timeline)) {
						$valueNew = $valueDeath = $valueInfections = 0;
						foreach ($dateList as $date) {
							if ($newValue = $this->searchValue($dataByPathKey, $date->label)) {
								$valueNew = $newValue->new_confirmed; // New cases
								$valueInfections = $newValue->confirmed; // Infections
								$valueDeath = $newValue->deaths; // Deaths
							}
							// New cases
							if (!array_key_exists($date->label, $dataNew)) {
								$dataNew = array_merge($dataNew, array($date->label => array(label => date('M j', strtotime($date->label)), value => $valueNew)));
							} else {
								$dataNew[$date->label][value] = $dataNew[$date->label][value] + $valueNew;
							}
							//Infections
							if (!array_key_exists($date->label, $dataInfections)) {
								$dataInfections = array_merge($dataInfections, array($date->label => array(label => date('M j', strtotime($date->label)), value => $valueInfections)));
							} else {
								$dataInfections[$date->label][value] = $dataInfections[$date->label][value] + $valueInfections;
							}
							// Deaths
							if (!array_key_exists($date->label, $dataDeaths)) {
								$dataDeaths = array_merge($dataDeaths, array($date->label => array(label => date('M j', strtotime($date->label)), value => $valueDeath)));
							} else {
								$dataDeaths[$date->label][value] = $dataDeaths[$date->label][value] + $valueDeath;
							}
						}					
					}
				} else {
					$this->writeLog('makedataForCumulativeValues = ' .$pathesList['path'].'/'.$key);
				}
			}
			
			// New cases
			if (!empty($dataNew)) {$dataSetNew = $this->prepareCumulativeValuesData($dataNew);}

			// Infections
			if (!empty($dataNew)) {$dataSetInfections = $this->prepareCumulativeValuesData($dataNew);}

			// Deaths
			if (!empty($dataDeaths)) {$dataSetDeath = $this->prepareCumulativeValuesData($dataDeaths);}
			$index++;
		}
		// New cases
		if ($dataSetNew && sizeof($dataSetNew)) {
			$this->saveFile(json_encode($dataSetNew), 'newCasesData');
		}
		// Infections
		if ($dataSetInfections && sizeof($dataSetInfections)) {
			$this->saveFile(json_encode($dataSetInfections), 'infectionsData');
		}
		// Deaths
		if ($dataSetDeath && sizeof($dataSetDeath)) {
			$this->saveFile(json_encode($dataSetDeath), 'deathsData');
		}
	}
	
	function prepareCumulativeValuesData($data)
	{
		$returnData = array();
		foreach ($data as $item) {
			$returnData[] = (object)$item;
		}
		
		return !empty($returnData) ? $returnData : false;
	}
	
	function writeLog($text) 
	{
	    $fname = 'cron.log';
	    $fileName = $this->getPathSave().'../../log/'.$fname;
	    $fh = fopen($fileName, "a") or die("Unable to open file! " .$fileName);
	    if (!is_writable($fileName)) {die("Unable write to file! " .$fileName);}
		fwrite($fh, date('Y-m-d G:i:s', time()). ' === '.$text. PHP_EOL);
		fclose($fh);
	}
}
