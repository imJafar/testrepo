import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)
export default new Vuex.Store({
	state: {
		country: 'nl' //default country
	},

	getters: {
		// Here we will create a getter
	},
  
	mutations: {
		// Here we will create Jenny
		changeCountry(state, key){
			state.country = key
		}
	},
  
	actions: {
		// Here we will create Larry
		changeCountry(context, key){
			context.commit('changeCountry', key)
		}
	}
});