import Vue from 'vue'
import App from './App.vue'

import VueFusionCharts from 'vue-fusioncharts'
import FusionCharts from 'fusioncharts'
import Maps from 'fusioncharts/fusioncharts.maps'
import Powercharts from 'fusioncharts/fusioncharts.powercharts'
import WorldWithCountries from 'fusioncharts/maps/fusioncharts.worldwithcountries'
import MapNetherlands from 'fusioncharts/maps/fusioncharts.netherlands'
import Charts from "fusioncharts/fusioncharts.charts";
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion'
import router from './router'
import store from './store'

import VueSlideUpDown from 'vue-slide-up-down'
Vue.component('slide-up-down', VueSlideUpDown)



Vue.use(VueFusionCharts, FusionCharts, Maps, Powercharts, WorldWithCountries, MapNetherlands, Charts, FusionTheme)

Vue.config.productionTip = false
Vue.prototype.$locale = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
