export const dataSource = {
  chart: {
    caption: "Infections - Cumulative",
	captionposition: "LEFT",
	legendposition: "BOTTOM",
    theme: "fusion",
	palettecolors: 'C73A52'
  },
  data: null
};

export let endpointComponent = 'assets/charts-data/json/infectionsData';