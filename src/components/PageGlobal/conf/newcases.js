export const dataSource = {
  chart: {
    caption: "New cases - Daily",
	captionposition: "LEFT",
    theme: "fusion",
	plotFillColor: "#BCE8D1",
	showPlotBorder: "1",
	plotBorderColor: "#40BA7D"
  },
  data: null
};

export let endpointComponent = 'assets/charts-data/json/newCasesData';