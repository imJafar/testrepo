export const dataSource = {
  chart: {
    caption: "Deaths - Cumulative",
	captionposition: "LEFT",
	legendposition: "BOTTOM",
    theme: "fusion",
	palettecolors: '443D3E'
  },
  data: null
};

export let endpointComponent = 'assets/charts-data/json/deathsData';