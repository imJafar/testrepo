export const dataSource = {
  chart: {
    caption: "Infections per region - Comulative",
	captionposition: "LEFT",
	legendposition: "right",
	legendBgColor: "#FFF",
	legendBgAlpha: "100",
	legendFontColor: "#B1B1B1",
	legendBorderColor: "#C8435E",
	legendShadow: "1",
	legendIconSides: "4",
	doughnutRadius: "85",
	showValues: "0",
	showLabels: "0",
    theme: "fusion"
  },
  data: [
    {
		label: "China",
		value: "1000",
		color: '#433C3D'
    },
    {
		label: "Outside China",
		value: "5300",
		color: '#C83B52'
    },
    {
		label: "Europe",
		value: "10500",
		color: '#41A658'
    },
    {
		label: "Americas",
		value: "18900",
		color: '#5496A5'
    },
    {
		label: "Other",
		value: "17904",
		color: '#857972'
    }
  ]
};

export let endpointComponent = 'assets/charts-data/json/infectionsRegionsData';