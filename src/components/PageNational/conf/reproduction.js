export const dataSource = {
  chart: {
    caption: "Reproduction - Fastest growth",
	captionposition: "LEFT",
	legendposition: "BOTTOM",
	showLabels: "1",
	showValues: "0",
	showLegend: "0",
    theme: "fusion"
  },
categories: [
    {
      category: [
        {
          label: "Jan 1"
        },
        {
          label: "Jan 2"
        },
        {
          label: "Jan 3"
        },
        {
          label: "Jan 4"
        },
        {
          label: "Jan 5"
        },
        {
          label: "Jan 6"
        },
        {
          label: "Jan 7"
        }
      ]
    }
  ],
  dataset: [
    {
      seriesname: "United State",
      data: [
        {
          value: "55"
        },
        {
          value: "45"
        },
        {
          value: "52"
        },
        {
          value: "29"
        },
        {
          value: "48"
        },
        {
          value: "28"
        },
        {
          value: "32"
        }
      ]
    },
    {
      seriesname: "Italy",
      data: [
        {
          value: "44"
        },
        {
          value: "33"
        },
        {
          value: "36"
        },
        {
          value: "25"
        },
        {
          value: "29"
        },
        {
          value: "21"
        },
        {
          value: "30"
        }
      ]
    },
    {
      seriesname: "Brazil",
      data: [
        {
          value: "50"
        },
        {
          value: "30"
        },
        {
          value: "49"
        },
        {
          value: "22"
        },
        {
          value: "43"
        },
        {
          value: "14"
        },
        {
          value: "31"
        }
      ]
    }
  ]
};

export let endpointComponent = 'assets/charts-data/json/national/nl/reproductionData';