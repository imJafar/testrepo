export const dataSource = {
chart: {
	caption: "Population fatality rate",
	captionposition: "LEFT",
	theme: "fusion",
	numbersuffix: "%",
	},
categories: [
    {
      category: [
        {
          label: "1"
        },
        {
          label: "2"
        },
        {
          label: "3"
        },
        {
          label: "4"
        },
        {
          label: "5"
        },
        {
          label: "6"
        },
        {
          label: "7"
        },
        {
          label: "8"
        },
        {
          label: "9"
        }
      ]
    }
  ],
  dataset: [
    {
      linethickness: "3",
      data: [
        {
          value: "1"
        },
        {
          value: "1.2"
        },
        {
          value: "1.8"
        },
        {
          value: "2"
        },
        {
          value: "2.8"
        },
        {
          value: "3.6"
        },
        {
          value: "3"
        },
        {
          value: "3.4"
        },
        {
          value: "3.9"
        }
      ]
    }
  ]
};

export let endpointComponent = 'assets/charts-data/json/national/nl/populationFatalityData';