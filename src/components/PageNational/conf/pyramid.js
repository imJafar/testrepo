export const dataSource = {
  chart: {
    caption: "Viral Pyramid",
	captionposition: "LEFT",
	theme: "fusion",
	decimals: "1",
    showvalues: "1",
	plotfillalpha: "70",
	streamlineddata: "0"
  },
   data: [
    {
      label: "Death",
      value: "3.315"
    },
    {
      label: "Hospitalizations",
      value: "9.309"
    },
    {
      label: "Tested positrive",
      value: "29.214"
    },
    {
      label: "Infected",
      value: "500.000"
    }
  ]
};

export const endpointComponent = 'assets/charts-data/json/national/nl/viralpyramidData';