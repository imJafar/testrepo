export const dataSource = {
  chart: {
    caption: "Case fatality rate - Ongoing",
	captionposition: "LEFT",
	legendposition: "BOTTOM",
    theme: "fusion",
	numbersuffix: "%",
	legendPosition: "absolute",
	legendXPosition: "100",
	legendYPosition: "60",
	legendBgColor: "#FFF",
	legendBgAlpha: "100",
	legendFontColor: "#B1B1B1",
	legendBorderColor: "#C8435E",
	legendShadow: "1",
	legendNumRows: "5",
	legendIconSides: "4",
  },
  categories: null,
  dataset: null
};

export let endpointComponent = 'assets/charts-data/json/national/fatalityRateDataAmericasEurope';