export let dataSource = {
	chart: {
		caption: "Pandemic - Cases per million",
		captionposition: "LEFT",
		legendposition: "BOTTOM",
		legendNumRows: "3",
		reverseLegend: "1",
		legendIconScale: "1.6",
		theme: "fusion"
	},
	colorrange: {
		gradient: "0",
		color: [
			{
				maxvalue: "1000000000",
				displayvalue: "1000+",
				code: "#9e3b4d"
			},
			{
				maxvalue: "1000",
				displayvalue: "100-1000",
				code: "#c8435e"
			},
			{
				maxvalue: "100",
				displayvalue: "10-100",
				code: "#ec848e"
			},
			{
				maxvalue: "10",
				displayvalue: "1-10",
				code: "#fec2c4"
			},
			{
				maxvalue: "1",
				displayvalue: ">0-1",
				code: "#ffe0e1"
			},
			{
                maxvalue: "0",
                displayvalue: "No data",
                code: "#e0e0e0"
			}
		]
	},
	data: null 
};

export let endpointComponent = 'assets/charts-data/json/national/nl/mapData';
