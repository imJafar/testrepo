export const dataSource = {
  chart: {
    caption: "Global death - Daily",
	captionposition: "LEFT",
	legendPosition: "absolute",
	legendXPosition: "80",
	legendYPosition: "60",
	legendBgColor: "#FFF",
	legendBgAlpha: "100",
	legendFontColor: "#B1B1B1",
	legendBorderColor: "#C8435E",
	legendShadow: "1",
	legendNumRows: "5",
    theme: "fusion",
	palettecolors: '443D3E'
  },
  categories: null,
  dataset: null
};

export let endpointComponent = 'assets/charts-data/json/national/nl/globalDeathsData';