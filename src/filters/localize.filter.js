// import store from '../store'
import en from '../assets/locales/en.json'
import nl from '../assets/locales/nl.json'
import ru from '../assets/locales/ru.json'

const locales = {
    'en-US': en,
    'nl-NL': nl,
    'ru-RU': ru,
}
/*
export default {
    props: ['locale'],
    filters: {
        localizeFilter: function(key) {

            return locales[locale][key] || `[Localize error] key ${key} not found`
        }
    }
}
*/
export default function localizeFilter (key) {
    console.log(this.$locale)
    return locales[this.$locale][key] || `[Localize error] key ${key} not found`
}
