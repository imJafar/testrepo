import Vue from 'vue'
import Router from 'vue-router'
import Global from '@/components/PageGlobal'
import National from '@/components/PageNational'
import Local from '@/components/PageLocal'
import Testing from '@/components/PageTesting'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'PageGlobal',
            component: Global
        },
        {
            path: '/national',
            name: 'PageNational',
            component: National
        },
        {
            path: '/local',
            name: 'PageLocal',
            component: Local
        },
        {
            path: '/testing',
            name: 'PageTesting',
            component: Testing
        }
    ]
})